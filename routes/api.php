<?php

use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductApiController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('order', [OrderController::class, 'index']);

Route::get('order/{order}', function($orderid) {
	$resp = new OrderController;
	return $resp->show($orderid);
});

Route::post('order', function(Request $request) {
	$resp = new OrderController;
	return $resp->store($request);
});

Route::put('order/updatestatus/{order}', function(Request $request, $orderid) {
	$resp = new OrderController;
	return $resp->update($request, $orderid);
});


Route::get('products', [ProductApiController::class, 'index']);
Route::get('product/{product}', [ProductApiController::class, 'show']);

Route::post('products', function(Request $request) {
	$resp = new ProductApiController;
	return $resp->store($request);
});

Route::put('products/{product}', function(Request $request, $productId) {
	$resp = new ProductApiController;
	$product = $resp->findOrFail($productId);
	return $resp->update($request, $product);
});

Route::delete('products/{product}', [ProductApiController::class, 'delete']);