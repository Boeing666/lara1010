<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersProducts extends Model
{
	protected $fillable = ['order_id', 'product_id', 'count'];
}
