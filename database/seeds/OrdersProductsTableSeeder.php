<?php

use Illuminate\Database\Seeder;

use App\Order;
use App\Product;
use App\OrdersProducts;

class OrdersProductsTableSeeder extends Seeder
{
	public function run()
    {
		$faker = \Faker\Factory::create();
		
		for ($i = 0; $i < 50; $i++) {
			Product::create([
				'name' => $faker->title,
				'description' => $faker->title,
				'price' => $faker->randomNumber(2),
				'img_url' => $faker->imageUrl()
			]);
		}

		for ($i = 0; $i < 50; $i++) {
			Order::create([
				'address' => $faker->address,
				'description' => $faker->title,
				'status' => $faker->randomNumber(1)
			]);
		}

		for ($i = 2; $i < 20; $i++) {
			OrdersProducts::create([
				'order_id' => $i,
				'product_id' => $i,
				'count' => $faker->randomNumber(1)
			]);
		}
	}
}