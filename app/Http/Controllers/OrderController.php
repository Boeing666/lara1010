<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\OrdersProducts;
use App\Product;
use App\Order;

class OrderController extends Controller
{
	public function index()
    {
        return OrdersProducts::all();
    }
 
    public function show($orderid)
    {
		$orders = DB::table('orders_products')->where('order_id', '=', $orderid)->get();
		
		$response = [];
		foreach ($orders as $value) 
		{
			$product = DB::table('products')->where('id', '=', $value->id)->limit(1)->get();
			array_push($response,$product);
		}

        return response()->json($response, 200);
    }
 
    public function store(Request $request)
    {
		$order = new Order();
		$order->address = $request->get('order_address');
		$order->description = $request->get('order_description');
		$order->status = 1;
		$order->save();

		$response = [];
		$products = $request->get('products');
		foreach ($products as $key => $value) 
		{
			$finaly = new OrdersProducts();
			$finaly->order_id = (int)$order->id;
			$finaly->product_id = (int)$key;
			$finaly->count = (int)$value;
			$finaly->save();
			
			array_push($response,$finaly);
		}

        return response()->json($response, 201);
    }
	
	public function findOrFail($productId)
    {
        return OrdersProducts::findOrFail($productId);
    }

    public function update(Request $request, $orderid)
    {
		$resp = Order::findOrFail($orderid);

        $resp->status = (int)$request->get('status');
        $resp->save();
 
        return response()->json($resp, 200);
    }
}